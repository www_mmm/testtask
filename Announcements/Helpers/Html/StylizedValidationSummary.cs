using System;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace Announcements.Helpers.Html
{
    public static class StylizedValidationSummary
    {
        public static MvcHtmlString ThemedValidationSummary(this HtmlHelper source,
            string divClass = "", string validationMessage = "Errors:")
        {
            var stringBuilder = new StringBuilder();

            if (source.ViewData.ModelState.IsValid)
                return MvcHtmlString.Create(String.Empty);
            stringBuilder.Append("<div class='"+divClass+"'>");
            stringBuilder.Append("<div class='text-left summary-errors alert alert-danger'>");

            stringBuilder.Append(
                "<button type='button' class='close' aria-label='Close' data-dismiss='alert'><span aria-hidden='true'>x</span></button>");

            stringBuilder.Append(String.Format("<p>{0}</p>", validationMessage));

            stringBuilder.Append("<ul>");

            stringBuilder.Append(
                String.Join("", source.ViewData.ModelState
                    .SelectMany(error => error.Value.Errors)
                    .Select(error =>new{ErrorMessage= error.ErrorMessage})
                    .Distinct()
                    .Select(error => String.Format("<li><span>{0}</span></li>", error.ErrorMessage))));

            stringBuilder.Append("</ul>");
            stringBuilder.Append("</div>");
            stringBuilder.Append("</div>");
            return MvcHtmlString.Create(stringBuilder.ToString());
        }
    }
}
