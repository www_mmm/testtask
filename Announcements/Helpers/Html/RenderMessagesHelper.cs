﻿using System.Collections;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using Announcements.Helpers.Classes;

namespace Announcements.Helpers.Html
{
    public static class RenderMessagesHelper
    {
        public static IHtmlString RenderMessages<T>(this HtmlHelper<T> helper, string divClass = "")
        {
            return helper.Partial("_Messages", divClass);
        }
    }

    public static class GlobalMessages
    {
        public static void AddErrorMessage(HttpSessionStateBase session, string message)
        {
            if (session != null)
            {
                var messages = (ArrayList)session["pageMessages"];
                if (messages == null)
                {
                    messages = new ArrayList();
                }
                messages.Add(new ErrorMessage(message));
                session["pageMessages"] = messages;
            }
        }

        public static void AddSuccessMessage(HttpSessionStateBase session, string message)
        {
            if (session != null)
            {
                var messages = (ArrayList)session["pageMessages"];
                if (messages == null)
                {
                    messages = new ArrayList();
                }
                messages.Add(new SuccessMessage(message));
                session["pageMessages"] = messages;
            }
        }


        public static void AddWarningMessage(HttpSessionStateBase session, string message)
        {
            if (session != null)
            {
                var messages = (ArrayList)session["pageMessages"];
                if (messages == null)
                {
                    messages = new ArrayList();
                }
                messages.Add(new WarnMessage(message));
                session["pageMessages"] = messages;
            }
        }
    }
}
