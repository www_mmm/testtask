﻿using System;
using System.Web.Mvc;

namespace Announcements.Helpers.Attributes
{
    public class RolesAttribute : AuthorizeAttribute
    {
        public RolesAttribute(params Enums.Enums.Role[] roles)
        {
            Roles = String.Join(",", roles);
        }
    }
}