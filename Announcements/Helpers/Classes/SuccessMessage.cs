﻿using System;

namespace Announcements.Helpers.Classes
{
    [Serializable]
    public class SuccessMessage : PageMessage
    {
        public SuccessMessage() : base("successMessage", "")
        {
        }

        public SuccessMessage(string msg) : base("successMessage", msg)
        {
        }
    }
}