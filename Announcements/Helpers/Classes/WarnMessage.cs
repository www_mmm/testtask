﻿using System;

namespace Announcements.Helpers.Classes
{
    [Serializable]
    public class WarnMessage : PageMessage
    {
        public WarnMessage() : base("warnMessage", "")
        {
        }

        public WarnMessage(string msg) : base("warnMessage", msg)
        {
        }
    }
}