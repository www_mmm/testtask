﻿using System;

namespace Announcements.Helpers.Classes
{
    [Serializable]
    public class ErrorMessage : PageMessage
    {
        public ErrorMessage() : base("errorMessage", "")
        {
        }

        public ErrorMessage(string msg) : base("errorMessage", msg)
        {
        }
    }
}