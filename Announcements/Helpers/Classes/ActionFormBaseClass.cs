﻿using System;

namespace Announcements.Helpers
{
    public class ActionFormBaseClass
    {
        public Enums.Enums.FormActions? FormAction { get; set; }
        public string FormActionValue { get; set; }

        public int? FormActionValueInt
        {
            get
            {
                if (int.TryParse(FormActionValue, out var result))
                {
                    return result;
                }
                return null;
            }
        }

        public Guid? FormActionValueGuid
        {
            get
            {
                if (Guid.TryParse(FormActionValue, out var result))
                {
                    return result;
                }
                return null;
            }
        }
    }
}