﻿using System;

namespace Announcements.Helpers.Classes
{
    [Serializable]
    public class PageMessage
    {
        private string _cssClass;
        private string _message;

        public PageMessage()
        {
        }

        public PageMessage(string msg)
        {
            _message = msg;
        }

        public PageMessage(string cssClass, string msg)
        {
            _cssClass = cssClass;
            _message = msg;
        }

        public virtual string Message
        {
            get
            {
                return _message;
            }
            set
            {
                _message = value;
            }
        }

        public virtual string CssClass
        {
            get
            {
                return _cssClass;
            }
        }

        public override string ToString()
        {
            return Message;
        }

        public static PageMessage Success(string msg)
        {
            return new SuccessMessage(msg);
        }

        public static PageMessage Error(string msg)
        {
            return new ErrorMessage(msg);
        }

        public static PageMessage Warning(string msg)
        {
            return new WarnMessage(msg);
        }
    }
}