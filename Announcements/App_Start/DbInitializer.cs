﻿using System.Data.Entity;
using Announcements.Models;
using Announcements.Models.DbContext;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Announcements
{
    public class DbInitializer : CreateDatabaseIfNotExists<AnnouncementsContext>
    {
        protected override void Seed(AnnouncementsContext context)
        {
            var userManager = new ApplicationUserManager(new UserStore<ApplicationUser>(context));
            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));

            var identityRole = new IdentityRole { Name = "admin" };
            roleManager.Create(identityRole);

            var admin = new ApplicationUser { Email = "admin@gmail.com", UserName = "Admin" };
            string password = "123456";
            var result = userManager.Create(admin, password);

            if (result.Succeeded)
            {
                userManager.AddToRole(admin.Id, identityRole.Name);
            }

            base.Seed(context);
        }
    }
}