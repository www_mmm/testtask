﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Announcements.Models.ViewModels
{
    public class AnnouncementViewModel
    {
        public Guid? AnnouncementId { get; set; }

        [Required]
        [Display(Name = "Date")]
        public DateTime? DateTime { get; set; }

        [Required]
        [Display(Name = "Name")]
        public string Name { get; set; }
    }
}