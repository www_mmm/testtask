﻿using System.Web.Mvc;

namespace Announcements.Models.ViewModels
{
    public class CommonController : Controller
    {
        public ActionResult GetMessages()
        {
            return PartialView("_Messages");
        }
    }
}