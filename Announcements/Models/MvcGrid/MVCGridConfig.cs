using System;
using System.Linq;
using Announcements.Models.DbContext;
using Announcements.Models.MvcGrid;
using MVCGrid.Models;
using MVCGrid.Web;

[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(MVCGridConfig), "RegisterGrids")]

namespace Announcements.Models.MvcGrid
{
    public static class MVCGridConfig
    {
        public static void RegisterGrids()
        {
            #region AnnouncementGrid

            MVCGridDefinitionTable.Add("AnnouncementGrid", new MVCGridBuilder<Announcement>()
                .WithAuthorizationType(AuthorizationType.AllowAnonymous)
                .AddColumns(cols =>
                {
                    cols.Add().WithColumnName("Name")
                        .WithHeaderText("Name")
                        .WithSorting(true)
                        .WithValueExpression(i => i.Name);

                    cols.Add()
                        .WithColumnName("DateTime")
                        .WithHeaderText("Date")
                        .WithSorting(true)
                        .WithValueExpression(i => i.DateTime.ToShortDateString());

                    cols.Add().WithColumnName("Edit")
                        .WithSorting(false)
                        .WithHeaderText(" ")
                        .WithValueTemplate(
                            "<a onclick='Announcement.EditAnnouncement($(this))' " +
                            "class='btn btn-xs btn-primary mr-5' " +
                            "role='button' " +
                            "url='/Announcement/Edit' " +
                            "ajax-container-dest = '#myModalContent' " + 
                            "form-action-value = '{Model.AnnouncementId}' " +
                            "modal-success-callback = 'Announcement.Refresh()' " +
                            "callback = 'Announcement.Refresh()' " +
                            "is-modal = 'true'" +
                            "'>Edit</a>" +

                            "<a onclick='Announcement.DeleteAnnouncement($(this))' " +
                            "class='btn btn-xs btn-danger' " +
                            "url='/Announcement/Delete' " +
                            "confirm-message = 'Are you sure you want to delete the Announcement?' " +
                            $"form-action = '{Enums.Enums.FormActions.Delete.ToString()}' " +
                            "form-action-value = '{Model.AnnouncementId}' " +
                            "modal-success-callback = 'Announcement.Refresh()' " +
                            "role='button' " +
                            ">Delete</a>",
                            false);
                })
                .WithSorting(sorting: true, defaultSortColumn: "DateTime", defaultSortDirection: SortDirection.Dsc)
                .WithRowCssClassExpression(p => p.DateTime.Date < DateTime.Now.Date ? "success":"info")
                .WithPaging(paging: true, itemsPerPage: 10)
                .WithRetrieveDataMethod((context) =>
                {
                    using (var db = new AnnouncementsContext())
                    {
                        var options = context.QueryOptions;
                        string sortColumn = options.GetSortColumnData<string>();
                        var rowsCount = db.Announcements.Count(w => w.IsActive);
                        var orderExpr = options.SortColumnName + (options.SortDirection == SortDirection.Asc
                                            ? ""
                                            : " desc");
                        var query = db.Announcements.Where(w => w.IsActive);

                        if (!string.IsNullOrWhiteSpace(options.SortColumnName))
                        {
                            switch (options.SortColumnName.ToLower())
                            {
                                case "name":
                                    query = options.SortDirection == SortDirection.Asc
                                        ? query.OrderBy(p => p.Name)
                                        : query.OrderByDescending(p => p.Name);
                                    break;
                                case "datetime":
                                    query = options.SortDirection == SortDirection.Asc
                                        ? query.OrderBy(p => p.DateTime)
                                        : query.OrderByDescending(p => p.DateTime);
                                    break;
                            }
                        }

                        query = query.Skip(options.GetLimitOffset().Value).Take(options.GetLimitRowcount().Value);

                        return new QueryResult<Announcement>()
                        {
                            Items = query.ToList(),
                            TotalRecords = rowsCount
                        };
                    }
                })
            );

            #endregion
        }
    }
}