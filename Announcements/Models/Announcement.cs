﻿using System;

namespace Announcements.Models
{
    public class Announcement
    {
        public Guid AnnouncementId { get; set; }
        public DateTime DateTime { get; set; }
        public string Name { get; set; }
        public bool IsActive { get; set; }
    }
}