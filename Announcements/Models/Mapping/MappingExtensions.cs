﻿using Announcements.Models.ViewModels;

namespace Announcements.Models.Mapping
{
    public static class MappingExtensions
    {
        public static TDestination MapTo<TSource, TDestination>(this TSource source)
        {
            return AutoMapperConfiguration.Mapper.Map<TSource, TDestination>(source);
        }

        public static TDestination MapTo<TSource, TDestination>(this TSource source, TDestination destination)
        {
            return AutoMapperConfiguration.Mapper.Map(source, destination);
        }

        #region Entities Extensions
        public static AnnouncementViewModel ToModel(this Announcement entity)
        {
            return entity.MapTo<Announcement, AnnouncementViewModel>();
        }

        public static Announcement ToEntity(this AnnouncementViewModel model)
        {
            var entity = model.MapTo<AnnouncementViewModel, Announcement>();
            entity.IsActive = true;
            return entity;
        }
        #endregion

    }
}