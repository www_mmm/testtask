﻿var Announcement = Announcement ||
{
    EditAnnouncement: function(link) {
        AjaxForms.ActionLinks(link);
    },

    DeleteAnnouncement: function(link) {
        AjaxForms.ActionLinks(link);
    },

    Refresh: function() {
        MVCGrid.reloadGrid('AnnouncementGrid');
        AjaxForms.UpdateMessages();
    }
};

AjaxForms.AddCustomInitFunction(
    function CustomInit() {
        $('.input-group.date').datepicker({
            autoclose: true,
            todayHighlight: true,
            forceParse: true
        });
    });