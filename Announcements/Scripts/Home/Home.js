﻿var Home = Home ||
{
    EventsDates: [],
    
    ShowDay: function(date) {
        if (Home.EventsDates.indexOf(date.getTime()) >= 0) {
            return {
                tooltip: 'Contains Announcements',
                classes: 'has-events'
            };
        }
    },

    ChangeDate: function (e) {
        Home.PopulateAnnouncements(e.date);
    },

    ChangeMonth: function(e) {
        Home.PopulateAnnouncementsDates(e.date);
    },

    PopulateAnnouncementsDates: function(currentMonthDate) {
        $.ajax({
            url: "/Home/GetDatesContainsAnnouncements",
            success: function(result) {
                Home.EventsDates = result.map(m => new Date(m.match(/\d+/)[0] * 1).getTime());
            },
            async: false,
            data: {
                date: currentMonthDate.toISOString()
            }
        });
    },

    PopulateAnnouncements: function (date) {
        $.ajax({
            url: "/Home/GetAnnouncements",
            success: function (result) {
                $("#announcements").html(result);
            },
            data: {
                date: date.toISOString()
            }
        });
    }
};

AjaxForms.AddCustomInitFunction(
    function() {
        Home.PopulateAnnouncementsDates(new Date());
        $('div.calendar').datepicker({
                keyboardNavigation: false,
                todayHighlight: true,
                beforeShowDay: Home.ShowDay
            }).on('changeMonth', Home.ChangeMonth)
            .on('changeDate', Home.ChangeDate);

        $('div.calendar').datepicker('update', new Date());
    }
);