﻿var AjaxForms = AjaxForms ||
{
    InitForms: function() {
        $(".action-link").off("click");
        $(".action-link").on("click",
            function() {
                AjaxForms.ActionLinks($(this));
            });
        $(".change-input").off("change");
        $(".change-input").on("change",
            function() {
                AjaxForms.ActionLinks($(this));
            });

        $('[class*="-button"],[class^="btn"]').removeAttr('disabled');

        if (typeof window.CustomInitForm !== 'undefined') {
            if (window.CustomInitForm.constructor === Array) {
                for (var i = 0; i < window.CustomInitForm.length; i++) {
                    window.CustomInitForm[i]();
                }
            }
        }
    },

    ActionLinks: function(object) {
        if (!object) {
            object = $(this);
        }

        if (object.attr("disabled")) {
            return false;
        }

        //confirm
        var confirmMessageIf = $(object).attr("confirm-message-if");
        var confirmMessage = $(object).attr("confirm-message");
        var confirmMessageCancelButton = $(object).attr("confirm-message-cancel-button");
        var confirmMessageCancelButtonClass = $(object).attr("confirm-message-cancel-button-class");
        var confirmMessageOkButton = $(object).attr("confirm-message-ok-button");
        var confirmMessageOkButtonClass = $(object).attr("confirm-message-ok-button-class");
        var confirmTitle = $(object).attr("confirm-title");

        if (confirmMessageIf) {
            if ($("#" + confirmMessageIf).val() !== "true") {
                confirmMessage = null;
            }
        }

        //formAction
        var formAction = $(object).attr("form-action");
        if (!formAction) {
            formAction = "";
        }

        //formActionValue 
        var formActionValue = $(object).attr("form-action-value");
        if (!formActionValue) {
            formActionValue = "";
        }

        var submitGeneral = $(object).attr("submit-general");


        //containers                   
        var container = $(object).attr("ajax-container-dest");

        if (!container || container.length === 0) {
            container = $(object).closest(".ajax-container");
        }

        if (!container || container.length === 0) {
            container = $(object).closest("[ajax-container]");
        }


        if (!container || container.length === 0) {
            container = $(object).closest("#myModalContent");
        }

        //submit container
        var submitContainer = $(object).attr("submit-container");
        if (!submitContainer || submitContainer.length === 0) {
            submitContainer = $(object).closest(".ajax-container");
        }

        if (!submitContainer || submitContainer.length === 0) {
            submitContainer = $(object).closest("[ajax-container]");
        }

        if (!submitContainer || submitContainer.length === 0) {
            submitContainer = $(object).closest("#myModalContent");
        }

        //callBack
        var callBack = $(object).attr("callBack");

        //modalSuccessCallback
        var modalSuccessCallback = $(object).attr("modal-success-callback");

        var isModal = $(object).attr("is-modal");

        //url
        var url = $(object).attr("url");
        if (!url) {
            url = $(object).closest("form").attr("action");
        }

        if (confirmMessage) {
            AjaxForms.GenerateModalConfirm(confirmTitle,
                confirmMessage,
                [
                    {
                        title: confirmMessageOkButton,
                        cssClass: confirmMessageOkButtonClass,
                        callback: function() {
                            if (submitGeneral) {
                                AjaxForms.GeneralSubmitFormWithAction(object, url, formAction, formActionValue);
                            } else {
                                AjaxForms.SubmitFormWithAction(url,
                                    formAction,
                                    formActionValue,
                                    container,
                                    isModal,
                                    callBack,
                                    modalSuccessCallback,
                                    submitContainer);
                            }
                        }
                    }
                ],
                { title: confirmMessageCancelButton, cssClass: confirmMessageCancelButtonClass });
        } else {
            if (submitGeneral) {
                AjaxForms.GeneralSubmitFormWithAction(object, url, formAction, formActionValue);
            } else {
                AjaxForms.SubmitFormWithAction(url,
                    formAction,
                    formActionValue,
                    container,
                    isModal,
                    callBack,
                    modalSuccessCallback,
                    submitContainer);
            }
        }
        return false;
    },

    GenerateModalConfirm: function(confirmTitle, confirmMessage, confirmButtons, cancelButton) {
        var modalHtml =
            '<div id="confirm" style="z-index: 1061;" class="modal fade"><div class="modal-dialog" role="document">' +
                '<div class="modal-content">' +
                '<div class="modal-header"><h4>' +
                (confirmTitle ? confirmTitle : 'Confirm Delete') +
                '</h4></div><div class="modal-body"><p>' +
                (confirmMessage ? confirmMessage : '') +
                '</p></div><div class="modal-footer">';
        confirmButtons.forEach(function(button, index) {
            modalHtml += '<button type="button" id="confirm' +
                index +
                '" + data-dismiss="modal" class="btn ' +
                (button.cssClass ? button.cssClass : 'btn-danger') +
                '">' +
                (button.title ? button.title : "Delete") +
                '</button>';
        });

        modalHtml += '<button type="button" data-dismiss="modal" id="cancel" class="btn ' +
            (cancelButton.cssClass ? cancelButton.cssClass : 'btn-default') +
            '">' +
            (cancelButton.title ? cancelButton.title : 'Cancel') +
            '</button></div></div></div></div>';

        var modal = $(modalHtml);
        confirmButtons.forEach(function(button, index) {
            modal.one('click', '#confirm' + index, button.callback ? button.callback : function() {});
        });
        modal.one('click', '#cancel', cancelButton.callback ? cancelButton.callback : function() {});
        return modal.modal({
            backdrop: 'static',
            keyboard: false
        });
    },

    GeneralSubmitFormWithAction: function(object, url, formAction) {
        var formActionInput = $(document.createElement("input"));
        formActionInput.attr("name", "FormAction");
        formActionInput.attr("value", formAction);
        formActionInput.attr("type", "hidden");

        var currentForm = $(object).closest("form");

        var submitForm;

        if (currentForm.length > 0) {
            submitForm = currentForm;
        } else {
            submitForm = $('<form method="post"></form>');
            submitForm.appendTo('body');
        }

        if (url) {
            submitForm.attr("action", url);
        }

        var formIsAjax = false;
        if (submitForm.attr("data-ajax")) {
            submitForm.removeAttr("data-ajax");
            formIsAjax = true;
        }

        submitForm.append(formActionInput);


        submitForm.submit();
        setTimeout(function() {
            formActionInput.remove();
            AjaxForms.InitForms();
            if (formIsAjax) {
                submitForm.attr("data-ajax", true);
            }
        });
    },

    AddCustomInitFunction: function(func) {
        if (typeof window.CustomInitForm !== 'undefined') {
            if (window.CustomInitForm.constructor === Array) {
                window.CustomInitForm.push(func);
            } else {
                window.CustomInitForm = [];
                window.CustomInitForm.push(func);
            }
        } else {
            window.CustomInitForm = [];
            window.CustomInitForm.push(func);
        }
    },

    SubmitFormWithAction: function(controllerMethod,
        actionName,
        actionValue,
        container,
        isModal,
        callback,
        modalSuccessCallback,
        submitContainer) {
        $(submitContainer).find('[class*="-button"]').prop('disabled', true);
        $(submitContainer).find('[class*="-link"]').unbind('click');
        $(submitContainer).find('[class*="-link"]').unbind('change');

        var form = $(submitContainer).find("form");
        var formData = new FormData();
        if (form.prop("enctype") === "multipart/form-data") {
            form.find("input[type='file']")
                .each(function() {
                    for (var i = 0; i < this.files.length; i++) {
                        formData.append(this.name, this.files[i]);
                    }
                });
        }
        var formSerialize = $(form).serializeArray();

        $.each(formSerialize,
            function(key, input) {
                formData.append(input.name, input.value);
            });

        if (window.location.href.indexOf("_popup") >= 0) {
            formData.append("_popup", "true");
        }

        formData.append("FormAction", actionName);
        formData.append("FormActionValue", actionValue);

        $.ajax({
            url: controllerMethod,
            type: 'POST',
            processData: false,
            cache: false,
            contentType: false,
            data: formData,
            success: function(result) {
                if (result.success) {
                    $('#myModal').modal('hide');
                    if (typeof (window[modalSuccessCallback]) === 'function') {
                        window[modalSuccessCallback](result);
                    } else {
                        location.reload();
                    }
                } else {
                    $(container).html(result);
                    if (isModal) {
                        $('#myModal').modal({
                                keyboard: true
                            },
                            'show');
                    }
                    AjaxForms.InitForms();
                }
                if (typeof (window[callback]) === 'function') {
                    window[callback](result);
                }
            },
            error: function() {
                alert("Error!");
                AjaxForms.InitForms();
            }
        });
    },

    UpdateMessages: function () {
        $.ajax({
            url: "/Common/GetMessages",
            type: 'GET',
            success: function(result) {
                $("#global-messages").replaceWith(result);
            }
        });
    }
};


$(document).ready(function() {
    AjaxForms.InitForms();
});