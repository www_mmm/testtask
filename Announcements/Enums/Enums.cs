﻿namespace Announcements.Enums
{
    public class Enums
    {
        public enum Role
        {
            admin
        }

        public enum FormActions
        {
            Save,
            Delete,
            Move,
            Add,
            Parse,
            Stop,
            TabClick,
            Import,
            ChangeCustomer,
            SelectObject,
            Export,
            Filter,
            Change,
            DeleteItem,
            AddItem,
            MoveUp,
            MoveDown
        }
    }
}