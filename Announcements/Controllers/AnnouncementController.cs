﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using Announcements.Helpers;
using Announcements.Helpers.Attributes;
using Announcements.Helpers.Html;
using Announcements.Models.DbContext;
using Announcements.Models.Mapping;
using Announcements.Models.ViewModels;


namespace Announcements.Controllers
{
    [Roles(Enums.Enums.Role.admin)]
    public class AnnouncementController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Add()
        {
            var model = new AnnouncementViewModel();
            return PartialView("Partials/_Editor", model);
        }
        
        public ActionResult Edit(ActionFormBaseClass model)
        {
            if (model.FormActionValueGuid.HasValue)
            {
                try
                {
                    using (var db = new AnnouncementsContext())
                    {
                        var announcement = db.Announcements.FirstOrDefault(w => w.AnnouncementId == model.FormActionValueGuid);
                        if (announcement != null)
                        {
                            return PartialView("Partials/_Editor", announcement.ToModel());
                        }
                        else
                        {
                            ModelState.AddModelError("Error", "No Announcement");
                            return PartialView("Partials/_Editor", new AnnouncementViewModel());
                        }
                    }
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("Error", ex.Message);
                }
            }

            ModelState.AddModelError("Error","No Announcement");
            return PartialView("Partials/_Editor", new AnnouncementViewModel());
        }

        public ActionResult Save(AnnouncementViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return PartialView("Partials/_Editor", model);
            }

            try
            {
                using (var db = new AnnouncementsContext())
                {
                    var announcement = model.ToEntity();
                    if (model.AnnouncementId.HasValue)
                    {
                        db.Entry(announcement).State = EntityState.Modified;
                    }
                    else
                    {
                        announcement.AnnouncementId = Guid.NewGuid();
                        db.Announcements.Add(announcement);
                    }

                    db.SaveChanges();
                }
            }
            catch(Exception ex)
            {
                ModelState.AddModelError("UnknownError",ex.Message);
                return PartialView("Partials/_Editor", model);
            }
            GlobalMessages.AddSuccessMessage(Session, "Announcement has been saved.");
            return Json(new { success = true });
        }


        public ActionResult Delete(ActionFormBaseClass model)
        {
            if (model.FormActionValueGuid.HasValue)
            {
                try
                {
                    using (var db = new AnnouncementsContext())
                    {
                        var deleteAnnouncement =
                            db.Announcements.FirstOrDefault(w => w.AnnouncementId == model.FormActionValueGuid);
                        if (deleteAnnouncement != null)
                        {
                            deleteAnnouncement.IsActive = false;
                            db.Entry(deleteAnnouncement).State = EntityState.Modified;
                            db.SaveChanges();
                        }
                    }
                }
                catch (Exception ex)
                {
                    GlobalMessages.AddErrorMessage(Session, ex.Message);
                }
            }

            GlobalMessages.AddSuccessMessage(Session, "Announcement has been removed.");
            return Json(new { success = true });
        }

    }
}