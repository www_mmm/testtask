﻿using System;
using System.Linq;
using System.Web.Mvc;
using Announcements.Models.DbContext;
using Announcements.Models.Mapping;

namespace Announcements.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            using (var db = new AnnouncementsContext())
            {
                var model = db.Announcements.Where(w => w.IsActive).ToList().Select(s => s.ToModel()).ToList();
                return View(model);
            }
        }

        public ActionResult GetDatesContainsAnnouncements(DateTime? date)
        {
            var current = (date ?? DateTime.Now).Date;

            var startDate = new DateTime(current.AddMonths(-1).Year, current.AddMonths(-1).Month, 1);
            var endDate = new DateTime(current.AddMonths(2).Year, current.AddMonths(2).Month, 1).AddMilliseconds(-1);

            using (var db = new AnnouncementsContext())
            {
                var dates = db.Announcements.Where(w => w.IsActive && w.DateTime >= startDate && w.DateTime <= endDate)
                    .Select(s => s.DateTime).Distinct().ToList();
                return Json(dates, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GetAnnouncements(DateTime? date)
        {
            var current = (date ?? DateTime.Now).Date;
            using (var db = new AnnouncementsContext())
            {
                var model = db.Announcements.Where(w => w.IsActive && w.DateTime == current)
                    .OrderByDescending(o => o.Name)
                    .ToList().Select(s => s.ToModel()).ToList();

                return PartialView("Partials/_Announcements", model);
            }
        }
    }
}